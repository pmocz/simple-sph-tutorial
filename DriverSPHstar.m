%% == SPH code for Polytropic Star ==
%
%       Philip Mocz (2014)
%       AC274, Harvard University
%

%% Parameters
clear all;
M        = 2;                    % total mass
R        = 0.75;                 % radius
N        = 100;                  % number of particles
dt       = 0.04;                 % time step
n_tstep  = 200;                  % number of time steps
nu       = 1;                    % damping
k        = 0.1;                  % pressure constant
npoly    = 1;                    % polytropic index
m        = M/N;                  % particle mass
h        = 0.04/sqrt(N/1000);    % smoothing length
% acceleration of gravity parameter
lambda   = 2*k*pi^(-1/npoly)*(M*(1+npoly)/R^2)^(1+1/npoly)/M;

%% Initialize
rng(42);                        % seed the random number generator
phi = 2*pi*rand(N,1);
r = R*sqrt(rand(N,1));
x = [r.*cos(phi) r.*sin(phi)];  % unif. sphere radius R
v = zeros(N,2);                 % 0 velocities

rho = Density( x, m, h );                            % compute densities
P = k*rho.^(1+1/npoly);                              % compute pressures
a = Acceleration( x, v, m, rho, P, nu, lambda, h );  % compute acceleration

% get v at t=-0.5*dt for the leap frog integrator using Euler's method
v_mhalf = v - 0.5*dt*a;

%% Evolve
figure(1);
for i = 1:n_tstep
    % leap frog
    v_phalf = v_mhalf + a*dt;
    x = x + v_phalf*dt;
    v = 0.5*(v_mhalf+v_phalf);
    v_mhalf = v_phalf;
    % update densities, pressures, accelerations
    rho = Density( x, m, h );
    P = k*rho.^(1+1/npoly);
    a = Acceleration( x, v, m, rho, P, nu, lambda, h );
    % plot
    scatter(x(:,1),x(:,2),5,rho,'linewidth',6); cb = colorbar; ylabel(cb,'density');
    title(['Density at t=',num2str(dt*i)],'fontsize',14);
    xlabel('x','fontsize',14);
    ylabel('y','fontsize',14);
    axis([-1 1 -1 1]);
    caxis([0 2.5]);
    drawnow;
end

%% Post Process

% measure and plot density of final configuration
radii = linspace(0,R,100);
slices = 4;
density_final= zeros(slices,length(radii));
for j = 1:slices
    for i = 1:length(radii)
        density_final(j,i) = ProbeDensity( x, m, h, radii(i),(j-1)*2*pi/slices  );
    end
end
figure(2); hold on;
for j = 1:slices
    p1 = plot(radii, density_final(j,:),'b+','linewidth',1);
end
p2 = plot(radii,((lambda/(2*k*(1+npoly)))*(R^2-radii.^2)).^npoly,'r--','linewidth',2);
title('Density profile of polytrope in equilibrium','fontsize',12);
xlabel('r','fontsize',12);
ylabel('\rho(r)','fontsize',12);
legend([p1 p2],{'SPH','theory'});
hold off;

%% plot kernel
h = 1;
r = [linspace(-3*h,3*h,1000)' zeros(1000,1)];
figure;  plot( r(:,1), kernel( r, h ), 'b','linewidth',3);
gk = gradkernel( r, h );
hold on; plot( r(:,1), gk(:,1), 'r','linewidth',3);
plot( r(:,1), lapkernel( r, h ), 'g','linewidth',3);
title('SPH Kernel','interpreter','latex','fontsize',14);
axis([-3*h 3*h -3 2]);
xlabel('q','interpreter','latex','fontsize',14);
ylabel('y','interpreter','latex','fontsize',14);
legend('W(r)','|\nabla W(r)|','\nabla^2 W(r)');
hold off;

