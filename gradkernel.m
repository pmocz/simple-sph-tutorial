function DW = gradkernel( r, h )
%GRADKERNEL returns the gradient of the kernel function
%   see kernel.m

q = sqrt(sum(r.^2,2))/h;

DW = -5/(14*pi*h^2) * r;
fac = ( (3*(2-q).^2./(q*h^2)-12*(1-q).^2./(q*h^2)) .* (q<1) ...
       + 3*(2-q).^2./(q*h^2)                       .* (1<=q & q<2) );
fac(q==0) = 0;
DW = [fac fac] .* DW;
    
end

