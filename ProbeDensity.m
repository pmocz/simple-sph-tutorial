function [ rho ] = ProbeDensity( x, m, h, R, theta )
%PROBEDENSITY measure density at current timestep at inputed position (R,theta)
%   Input: particle positions x, mass m, scale length h,
%   distance R and angle theta in XY plane (if space is one dimensional,
%   theta is ignored), kernel type
%   Output: density at radius R, angle theta

% number of particles
N = length(x);

% initialize density
rho = 0;

% position at which to calculate density
pos = R*[cos(theta) sin(theta)];

% add the pairwise contributions to density
for j = 1:N
    % calculate vector between two particles
    uij = pos - x(j,:);
    rho_ij = m*kernel( uij, h );
    rho = rho + rho_ij;
end

end

