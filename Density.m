function rho = Density( x, m, h )
%DENSITY Compute density at each of the particles using smoothing kernel
%   Input: position, mass of each particle, scale length
%   Output: column vector of density at each particle

% number of particles
N  = length(x);

% initialize density with i=j contribution
rho = ones(N,1)*m*kernel( 0, h );

% add the pairwise contributions to density
for i = 1:N
%     % == vectorize ==
%     for j = i+1:N
%         % calculate vector between two particles
%         uij = x(i,:) - x(j,:);
%         % calculate contribution due to neighbors
%         rho_ij = m*kernel( uij, h );
%         % accumulate contributions to the density
%         rho(i) = rho(i) + rho_ij;
%         rho(j) = rho(j) + rho_ij;
%     end
%     % == below ==
    js = [ 1:i-1, i+1:N ];
    % calculate vector between two particles
    uij = -x(js,:);
    uij(:,1) = uij(:,1) + x(i,1);
    uij(:,2) = uij(:,2) + x(i,2);
    % calculate contribution due to neighbors
    rho_ij = m*kernel( uij, h );
    % accumulate contributions to the density
    rho(i) = rho(i) + sum(rho_ij);
end

end

