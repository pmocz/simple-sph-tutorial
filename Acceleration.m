function  a  = Acceleration( x, v, m, rho, P, nu, lambda, h )
%ACCELERATION calculate acceleration of each particle due to gravity, pressure, damping.

N = length(x);
a = zeros( N, 2 );

for i = 1:N
    % damping and gravity
    a(i,:) = a(i,:) - v(i,:)*nu - lambda*x(i,:);
%     % == vectorize ==
%     for j = i+1:N
%         % calculate vector between two particles
%         uij = x(i,:) - x(j,:);
%         % calculate acceleration due to pressure
%         pressure_a = -m*(P(i)/rho(i)^2+P(j)/rho(j)^2)*gradkernel( uij, h );
%         % accumulate the accelerations
%         a(i,:) = a(i,:) + pressure_a;
%         a(j,:) = a(j,:) - pressure_a;
%     end
%     % == below ==
    % pressure (pairwise calculation)
    js = [ 1:i-1, i+1:N ];
    % calculate vector between two particles
    uij = -x(js,:);
    uij(:,1) = uij(:,1) + x(i,1);
    uij(:,2) = uij(:,2) + x(i,2);
    % calculate acceleration due to pressure
    fac = -m*(P(i)/rho(i)^2+P(js)./rho(js).^2);
    pressure_a = gradkernel( uij, h );
    pressure_a = [fac,fac] .* pressure_a;
    % accumulate contributions to the density
    a(i,:) = a(i,:) + sum(pressure_a,1);
end

end