function W = kernel( r, h )
%KERNEL SPH cubic spline smoothing kernel (2D)
%   Input: positon vector r, scaling length h
%   Output: weight W

q = sqrt(sum(r.^2,2))/h;

W = 5/(14*pi*h^2) * ( ((2-q).^3-4*(1-q).^3) .* (q<1) ...
                     + (2-q).^3             .* (1<=q & q<2) );

end

