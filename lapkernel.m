function LW = lapkernel( r, h )
%LAPKERNEL returns the laplacian of the kernel function
%   see kernel.m

q = sqrt(sum(r.^2,2))/h;

LW = 5/(14*pi*h^2) * (  (3*(8-4./q-3*q)/h^2 - 12*(4-1./q-3*q)/h^2) .* (q<1) ...
                       + 3*(8-4./q-3*q)/h^2                       .* (1<=q & q<2) );

end
